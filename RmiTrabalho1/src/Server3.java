/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

/**
 *
 * @author aluno
 */
public class Server3 implements Raiz2 {

    private String host;

    public static void main(String[] args) {
        try {
            Server3 server = new Server3();
            if (args.length < 1) {
                server.host = "121.0.0.1";
            } else {
                server.host = args[0];
            }

            Raiz2 stubx = (Raiz2) UnicastRemoteObject.exportObject(server, 0);
            Registry reg = LocateRegistry.getRegistry(server.host);
            reg.rebind("//" + server.host + "/raiz2", stubx);
            System.err.println("Servidor carregado");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public double getx2(int a, int b, int c) throws RemoteException {
        double delta = b * b - 4 * (a * c);
        if (delta < 0 || a == 0) {
            return 0;
        }
        return (double) (-b) - Math.sqrt(delta) / (double) (2d * a);
    }

}

