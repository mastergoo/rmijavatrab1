/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

public class Cliente {

	public static void main(String[] args) {
		String host1 = args[0];
		String host2 = args[2];
		String host3 = args[1];
		try {
			Registry registry1 = LocateRegistry.getRegistry(host1);
			Registry registry2 = LocateRegistry.getRegistry(host2);
			Registry registry3 = LocateRegistry.getRegistry(host3);
			Sum stubsum = (Sum) registry1.lookup("//" + host1 + "/sum");
			Raiz1 stubraiz1 = (Raiz1) registry2.lookup("//" + host2 + "/raiz1");
			Raiz2 stubraiz2 = (Raiz2) registry3.lookup("//" + host3 + "/raiz2");
			double respsum = stubsum.getSum(Integer.parseInt(args[3]));
			int a = Integer.parseInt(args[4]);
			int b = Integer.parseInt(args[5]);
			int c = Integer.parseInt(args[6]);
			double respx1 = stubraiz1.getx1(a, b, c);
			double respx2 = stubraiz2.getx2(a, b, c);
			System.out.println("Sum = " + respsum + "\n" + "x1 = " + respx1 + "\n" + "x2 = " + respx2);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
